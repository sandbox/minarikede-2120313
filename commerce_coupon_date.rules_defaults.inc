<?php
/**
 * @file
 * commerce_coupon_date_backup.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_coupon_date_default_rules_configuration() {
  $items = array();
  $items['rules_commerce_coupon_date_validation'] = entity_import('rules_config', '{ "rules_commerce_coupon_date_validation" : {
    "LABEL" : "Commerce coupon validation: Date validation",
    "PLUGIN" : "reaction rule",
    "TAGS" : [ "coupon" ],
    "REQUIRES" : [ "rules", "commerce_coupon" ],
    "ON" : [ "commerce_coupon_validate" ],
    "IF" : [
      { "entity_has_field" : { "entity" : [ "coupon" ], "field" : "commerce_coupon_date" } },
      { "OR" : [
          { "NOT data_is" : {
              "data" : [ "coupon:commerce-coupon-date:value" ],
              "op" : "\u003C",
              "value" : [ "site:current-date" ]
            }
          },
          { "NOT data_is" : {
              "data" : [ "coupon:commerce-coupon-date:value2" ],
              "op" : "\u003E",
              "value" : [ "site:current-date" ]
            }
          }
        ]
      }
    ],
    "DO" : [
      { "drupal_message" : {
          "message" : "This coupon is available only between [coupon:commerce-coupon-date:value] - [coupon:commerce-coupon-date:value2].",
          "type" : "error",
          "repeat" : 0
        }
      },
      { "commerce_coupon_action_is_invalid_coupon" : [] }
    ]
  }
}');
  return $items;
}
